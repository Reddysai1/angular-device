﻿using Microsoft.EntityFrameworkCore;

namespace DevicesApi.Models
{
    public class DevicesDbContext : DbContext
    {
        public DbSet<Device> Devices { get; set; }

        public DevicesDbContext(DbContextOptions<DevicesDbContext> options) : base(options)
        {

        }
    }
}

