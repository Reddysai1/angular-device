﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevicesApi.Models
{
    public class DeviceFakeData
    {
        public static void Seed(IServiceProvider serviceProvider)
        {
            using var context = new DevicesDbContext(
          serviceProvider
          .GetRequiredService<DbContextOptions<DevicesDbContext>>());

            if (context.Devices.Any()) { return; }

            var devices = new List<Device>
            {
                new Device{ Id = "1", DeviceName="iPhone 13 Pro Max", Icon="iphone_13_pro_max_sierra_blue", Status = true, RelatedModelName="Apple", StorageCapacity="124GB"  },
                new Device{ Id = "2", DeviceName="iPhone 13 mini", Icon="iphone_13_mini_pink", Status = true, RelatedModelName="Apple", StorageCapacity="64GB"  },
                new Device{ Id = "3", DeviceName="iPhone 13 Pro", Icon="iphone_13_pro_gold", Status = false, RelatedModelName="Apple", StorageCapacity="248GB"  },
                new Device{ Id = "4", DeviceName="Samsung Galaxy S20 FE 5G", Icon="sgfe-cloud_navy-frontback-tiny", Status = true, RelatedModelName="Samsung", StorageCapacity="24GB"  },
                new Device{ Id = "5", DeviceName="Samsung Galaxy A52 5G", Icon="sm_a526_galaxya52_5g_awesome_black_front", Status = false, RelatedModelName="Samsung", StorageCapacity="124GB"  },
                new Device{ Id = "6", DeviceName="Samsung Galaxy S21 5G", Icon="samsung_galaxy_s21_phantom_white_frontback-tiny", Status = true, RelatedModelName="Samsung", StorageCapacity="64GB"  },
                new Device{ Id = "7", DeviceName="Motorola One 5G Ace", Icon="moto-one-5g-catalog", Status = true, RelatedModelName="Motorola", StorageCapacity="124GB"  },
                new Device{ Id = "8", DeviceName="Motorola G Power", Icon="2020_borneo_na_basic_flashgray_frontside_1", Status = true, RelatedModelName="Motorola", StorageCapacity="64GB"  },
                new Device{ Id = "9", DeviceName="HUAWEI P40", Icon="huawei-p40-black-backtoback-min", Status = true, RelatedModelName="HUAWEI", StorageCapacity="124GB"  },
            };

           context.Devices.AddRange(devices);

            context.SaveChanges();

        }
    }
}
