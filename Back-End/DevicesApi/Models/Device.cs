﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevicesApi.Models
{
    public class Device
    {
        public string Id { get; set; }
        public string DeviceName { get; set; }
        public string Icon { get; set; }
        public bool Status { get; set; }
        public string RelatedModelName { get; set; }
        public string StorageCapacity { get; set; }
    }
}
