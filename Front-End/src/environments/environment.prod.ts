const packageJson = require('../../package.json');

export const environment = {
  production: true,
  appName: 'DeviceApp',
  versions: {
    app: packageJson.version,
    angular: packageJson.dependencies['@angular/core'],
    angularCli: packageJson.devDependencies['@angular/cli'],
    typescript: packageJson.devDependencies['typescript'],
    rxjs: packageJson.dependencies.rxjs,
    ngrx: packageJson.dependencies['@ngrx/store'],
    bootstrap: packageJson.dependencies.bootstrap
  }, 
  api: {
	  baseUrl : 'https://localhost:44352/api/',
  },
  externalAppDomains: {
  }
};
