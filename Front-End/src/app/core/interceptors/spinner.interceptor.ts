//Angular
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';

//Rxjs
import { Observable } from 'rxjs';
import { delay, catchError, finalize } from 'rxjs/operators';

//Project
import { SpinnerService } from '../services/spinner.service';

@Injectable()
export class SpinnerInterceptor implements HttpInterceptor {
    private RequestCount = 0;

    constructor(private spinner: SpinnerService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        this.RequestCount = this.RequestCount + 1;
        if (this.RequestCount === 1) {
            this.spinner.showSpinner();
        }

        return next.handle(request).pipe(
            delay(200),
            finalize(() => {
                this.decreaseRequests();
            }),
            catchError((err) => {
                this.decreaseRequests();
                throw err;
            })
        );
    }

    private decreaseRequests() {
        this.RequestCount = this.RequestCount - 1;
        if (this.RequestCount === 0) {
            this.spinner.hideSpinner();
        }
    }
}