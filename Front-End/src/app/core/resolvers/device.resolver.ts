//Angular
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

//Rxjs
import { Observable } from 'rxjs';
import { filter, take } from 'rxjs/operators';

//Third Parties
import { select, Store } from '@ngrx/store';

//Project
import { getCommonState } from '../state/selectors/common.selector';
import * as fromRoot from '../core.state';
import { LoadDeviceList } from '../state/actions/common.action';

@Injectable({
  providedIn: 'root'
})
export class DeviceListResolver implements Resolve<any> {
  constructor(private rootStore: Store<fromRoot.AppState>) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    // Retrieve the user profile from the store.
    // If not in store, API will be called to retrieve it.
    // and "loaded" state is set to false.
    this.rootStore.dispatch(LoadDeviceList());
  }

}