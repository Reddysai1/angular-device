//Third parties
import { createSelector, createFeatureSelector } from '@ngrx/store';

//Project
import * as fromCommon from '../reducers/common.reducer';

//common selector
export const getCommonState = createFeatureSelector<fromCommon.State>('common');

export const selectDevicelist = createSelector(getCommonState,
    (state: fromCommon.State) => state.DeviceList ? state.DeviceList : fromCommon.initialState.DeviceList);

export const selectIsRelatedDeviceList = createSelector(getCommonState,
    (state: fromCommon.State) => state.isRelatedDeviceList ? state.isRelatedDeviceList : fromCommon.initialState.isRelatedDeviceList);

export const selectRelatedDevicesList = createSelector(getCommonState,
    (state: fromCommon.State) => state.RelatedDevices ? state.RelatedDevices : fromCommon.initialState.RelatedDevices);

export const selectedDeviceDetails = createSelector(getCommonState,
    (state: fromCommon.State) => state.deviceDetails ? state.deviceDetails : fromCommon.initialState.deviceDetails);
