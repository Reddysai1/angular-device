//Third Parties
import { Action, createReducer, on } from '@ngrx/store';
import { DeviceDTO } from '../../DTOs/device';

//Project
import * as fromCommom from '../actions/common.action';
import * as fromLoadash from 'lodash';

export interface State {
  DeviceList: DeviceDTO[];
  isRelatedDeviceList: boolean;
  RelatedDevices: DeviceDTO[];
  deviceDetails: null
}

export const initialState: State = {
  DeviceList: [],
  isRelatedDeviceList: false,
  RelatedDevices: [],
  deviceDetails: null
};

export const commonReducer = createReducer(
  initialState,

  on(fromCommom.LoadDeviceListSuccess, (state, { response }) => {

    return {
      ...state,
      DeviceList: response
    };
  }),
  on(fromCommom.UpdateDeviceList, (state, { payload }) => {

    let relatedDevices: DeviceDTO[] = [];
    let deviceListClone = fromLoadash.cloneDeep(state.DeviceList);

    //filter the related devices based on the related Model
    deviceListClone.map((list)=> {
      if(list.relatedModelName === payload.relatedModelName){
        relatedDevices.push(list)
      }
    });

    return {
      ...state,
      RelatedDevices: relatedDevices? relatedDevices: [],
      deviceDetails: payload,
      isRelatedDeviceList: true

    };
  }),

  on(fromCommom.ResetRelatedDeviceList, (state) => {

    let deviceListClone = fromLoadash.cloneDeep(state.DeviceList);
    return {
      ...state,
      RelatedDevices: [],
      DeviceList: deviceListClone,
      deviceDetails: null
    };
  }),

);

export function reducer(state: State | undefined, action: Action) {
  return commonReducer(state, action);
}