//Third Parties
import { props, createAction } from '@ngrx/store';


export const LoadDeviceList = createAction(
  '[LoadDeviceList] LoadDeviceList'
);

export const LoadDeviceListSuccess = createAction(
  '[LoadDeviceList] LoadDeviceList Success',
  props<{ response: any }>()
);

export const LoadDeviceListFailure = createAction(
  '[LoadDeviceList] LoadDeviceList Failure',
  props<{ error: any }>()
);

export const UpdateDeviceList = createAction(
  '[UpdateDeviceList] UpdateDeviceList',
  props<{ payload: any }>()
);

export const ResetRelatedDeviceList = createAction(
  '[ResetRelatedDeviceList] ResetRelatedDeviceList'
  );
