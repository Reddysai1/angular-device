//Angular
import { Injectable } from '@angular/core';

//Rxjs
import { map, mergeMap, catchError, tap, concatMap } from 'rxjs/operators';
import { of } from 'rxjs';

//Third Parties
import { Action } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';

//Project
import * as fromCommon from '../actions/common.action';
import { CommonService } from '../../services/common.service'


@Injectable()
export class CommonEffects {

  constructor(
    private actions$: Actions<Action>,
    private service: CommonService,

  ) { }


  LoadDeviceList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromCommon.LoadDeviceList),
      mergeMap(() => {
        return this.service.getDeviceList()
          .pipe(
            map((res) => {
              if (res) {
                return fromCommon.LoadDeviceListSuccess({ response: res })
              } else {
                return fromCommon.LoadDeviceListFailure({ error: res });
              }
            }),
            catchError((res) => {
              return of(fromCommon.LoadDeviceListFailure({ error: res }));
            })
          )
      })
    )
  );

 }