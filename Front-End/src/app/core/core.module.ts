//Angular
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule } from '@angular/common';

//Third Parties
import { StoreModule} from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  StoreRouterConnectingModule,
  RouterStateSerializer
} from '@ngrx/router-store';


//Project
import { reducers } from './core.state';
import { CustomSerializer } from './router/custom-serializer';
import { CommonEffects } from './state/effects/common.effect';
import { SpinnerInterceptor } from './interceptors/spinner.interceptor';



const PROVIDERS: any[] = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: SpinnerInterceptor,
    multi: true
  },
  {
    provide: RouterStateSerializer,
    useClass: CustomSerializer
  },
]

const MODULES_IMPORTED_EXPORTED: any[] = [
  BrowserAnimationsModule,
  HttpClientModule,
  CommonModule
]


@NgModule({
  declarations: [],
  imports: [
    ...MODULES_IMPORTED_EXPORTED,
    StoreModule.forRoot(reducers, {
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
      }
    }),
    EffectsModule.forRoot(
      [CommonEffects]),
    StoreRouterConnectingModule.forRoot()
  ],
  exports: [
    ...MODULES_IMPORTED_EXPORTED
  ],
  providers: [...PROVIDERS
  ]
})
export class CoreModule { }
