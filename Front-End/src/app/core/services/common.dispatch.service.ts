//Angular
import { Injectable } from '@angular/core';

//Third Parties
import { Store } from '@ngrx/store';

//Project
import * as fromRoot from '../core.state';
import { ResetRelatedDeviceList, UpdateDeviceList } from '../state/actions/common.action';


@Injectable({
    providedIn: 'root'
})
export class CommonDispatchService {

    constructor(private fromRootStore: Store<fromRoot.AppState>) { }

    UpdateDeviceList(payload: any){
        this.fromRootStore.dispatch(UpdateDeviceList({payload}))
    }

    ResetRelatedDeviceList(){
        this.fromRootStore.dispatch(ResetRelatedDeviceList());
    }


}
