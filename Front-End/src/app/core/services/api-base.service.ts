//Angular
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

//Project
import { environment } from '../../../environments/environment';

export const HEADERS_KEY = 'headers';

interface BaseHttpOptions {
  headers: HttpHeaders;
  params?: HttpParams;
}

export interface RequestConfig {
  withSpinner?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ApiBaseService {
  httpOptions: BaseHttpOptions;

  constructor(
    public http: HttpClient
  ) {
    this.httpOptions = this.generateBaseHeaders();
  }

  generateBaseHeaders(requestConfig: RequestConfig = { withSpinner: true }): BaseHttpOptions {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    };

    let customHeader: HttpHeaders = options[HEADERS_KEY];

    return {
      ...options,
      headers: customHeader
    };
  }

  /**
   * Builds the query params as an HttpParams from a given an Object
   * Unfortunately, Angular's HttpParams currently doesn't support non-string values.
   * https://github.com/angular/angular/issues/23856
   */
  buildQueryParams(source: any): HttpParams {
    let target: HttpParams = new HttpParams();
    Object.keys(source).forEach((key: string) => {
      const value: string | number | boolean | Date = source[key];
      if ((typeof value !== 'undefined') && (value !== null)) {
        target = target.append(key, value.toString());
      }
    });
    return target;
  }

  getApiURL(path: string): string {
    return `${environment.api.baseUrl}${path}`;
  }

  getMockURL(filename: string): string {
    return `${window.location.origin}/assets/mocks/${filename}`;
  }
}