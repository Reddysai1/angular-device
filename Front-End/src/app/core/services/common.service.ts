//Angular
import { Injectable } from "@angular/core";

//Rxjs
import { Observable } from "rxjs";

//Project
import { ApiBaseService } from "../services/api-base.service";
import * as fromAPI from "../const/api-end-points";
import { Device } from "../models/device.model";

@Injectable({
  providedIn: "root",
})
export class CommonService {
  constructor(private api: ApiBaseService) { }

  //lOGIN
  getDeviceList(): Observable<Device[]> {
    const options = {
      ...this.api.httpOptions,
      params: {},
    };
    return this.api.http.get<Device[]>(
      this.api.getApiURL(fromAPI.API_GET_DEVICE_LIST),
      options
    );
  }

}
