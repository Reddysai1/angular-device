//Angular
import { Injectable } from '@angular/core';

//Third Parties
import { NgxSpinnerService } from "ngx-spinner";

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {

  constructor(private spinner: NgxSpinnerService) { }

  /** Show spinner */
  showSpinner(): void {
    this.spinner.show();
  }

  /** Hide Spinner */
  hideSpinner(): void {
    this.spinner.hide();
  }

}
