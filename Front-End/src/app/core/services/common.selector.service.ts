//Angular
import { Injectable } from '@angular/core';

//Third Parties
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';

//Project
import {
  selectDevicelist, selectedDeviceDetails, selectIsRelatedDeviceList, selectRelatedDevicesList}
  from '../state/selectors/common.selector';

import * as fromRoot from '../core.state';


@Injectable({
  providedIn: 'root'
})
export class CommonSelectorService {

  constructor(private rootStore: Store<fromRoot.AppState>) { }

  getDeviceList$(): Observable<any> {
    return this.rootStore.pipe(select(selectDevicelist));
  }

  getIsRelatedList$(): Observable<any> {
    return this.rootStore.pipe(select(selectIsRelatedDeviceList));
  }

  getRelatedDeviceList$(): Observable<any> {
    return this.rootStore.pipe(select(selectRelatedDevicesList));
  }

  getselectedDeviceDetails$(): Observable<any> {
    return this.rootStore.pipe(select(selectedDeviceDetails));
  }

}