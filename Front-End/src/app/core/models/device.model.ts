export interface Device {
  id: string;
  deviceName: string;
  icon: string;
  status : any;
  relatedModelName: string;
  storageCapacity: string;
}