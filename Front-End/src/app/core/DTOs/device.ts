//Project
import { Device } from "../models/device.model";


export class DeviceDTO {
  public id = "";
  public deviceName = "";
  public icon = "";
  public status = "";
  public relatedModelName = "";
  public storageCapacity = "";

  constructor(model: Device) {
    this.id = model.id;
    this.deviceName = model.deviceName;
    this.icon = model.icon;
    this.status = model.status;
    this.relatedModelName = model.relatedModelName;
    this.storageCapacity = model.storageCapacity;
  }
}