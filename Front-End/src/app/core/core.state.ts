//Third Parties
import {
  ActionReducerMap,
  createFeatureSelector
} from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';

//Project
import { RouterStateUrl } from './router/router.state';
import * as fromCommon from './state/reducers/common.reducer';

export const reducers: ActionReducerMap<AppState> = {
  router: fromRouter.routerReducer,
  common: fromCommon.reducer
};

//Application state
export interface AppState {

  router: fromRouter.RouterReducerState<RouterStateUrl>;
  common: fromCommon.State;
}

export const selectRouterState = createFeatureSelector<AppState, fromRouter.RouterReducerState<RouterStateUrl>>('router');
export const {
  selectQueryParams,    // select the current route query params
  selectQueryParam,     // factory function to select a query param
  selectRouteParams,    // select the current route params
  selectRouteParam,     // factory function to select a route param
  selectRouteData,      // select the current route data
  selectUrl,            // select the current url
} = fromRouter.getSelectors(selectRouterState);
