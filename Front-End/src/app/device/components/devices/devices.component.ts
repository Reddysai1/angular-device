//Angular
import { Component, OnInit } from '@angular/core';

//Project
import { CommonSelectorService } from '@app/core/services/common.selector.service';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.scss']
})
export class DevicesComponent implements OnInit {

  deviceList$ = this.selector.getDeviceList$();
  deviceDetails$ = this.selector.getselectedDeviceDetails$();
  relatedDevices$ = this.selector.getRelatedDeviceList$();

  constructor(private selector: CommonSelectorService) { }

  ngOnInit(): void {
  }

}
