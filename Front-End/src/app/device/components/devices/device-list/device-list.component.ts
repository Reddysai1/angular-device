//Angular
import { Component, Input, OnInit } from '@angular/core';

//project
import { DeviceDTO } from '@app/core/DTOs/device';

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.scss']
})
export class DeviceListComponent implements OnInit {

  @Input() deviceList : DeviceDTO[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
