//Angular
import { Component, Input, OnInit } from '@angular/core';

//Project
import { CommonDispatchService } from '@app/core/services/common.dispatch.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input() list: any;
  @Input() type: string = '';
  searchText: string = '';

  constructor(private dispatchServcie: CommonDispatchService) { }

  ngOnInit(): void {
  }

  getDeviceDetails(device: any) {
    this.dispatchServcie.UpdateDeviceList(device);
  }

  CancelText() {
    this.searchText = '';
  }

}
