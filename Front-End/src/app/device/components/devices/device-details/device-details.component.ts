//Angular
import { Component, Input, OnInit } from '@angular/core';

//Project
import { CommonDispatchService } from '@app/core/services/common.dispatch.service';

@Component({
  selector: 'app-device-details',
  templateUrl: './device-details.component.html',
  styleUrls: ['./device-details.component.scss']
})
export class DeviceDetailsComponent implements OnInit {

  @Input() deviceDetails: any;
  @Input() relatedDevices: any;

  
  constructor(private dispatchService: CommonDispatchService) { }

  ngOnInit(): void {
  }

  goToDeviceList(){
    this.dispatchService.ResetRelatedDeviceList();
  }

}
