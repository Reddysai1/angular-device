//Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Project
import { DeviceRoutingModule } from './device-routing.module';
import { DeviceComponent } from './device.component';
import { SharedModule } from '../shared';
import { DevicesComponent } from './components/devices/devices.component';
import { DeviceListComponent } from './components/devices/device-list/device-list.component';
import { DeviceDetailsComponent } from './components/devices/device-details/device-details.component';
import { ListComponent } from './components/devices/list/list.component';


@NgModule({
  declarations: [
    DeviceComponent,
    DevicesComponent,
    DeviceListComponent,
    DeviceDetailsComponent,
    ListComponent,
  ],
  imports: [
    CommonModule,
    DeviceRoutingModule,
    SharedModule
  ]
})
export class DeviceModule { }
