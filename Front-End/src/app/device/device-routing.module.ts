//Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//Project
import { DeviceListResolver } from '../core/resolvers/device.resolver';
import { DevicesComponent } from './components/devices/devices.component';
import { DeviceComponent } from './device.component';

const routes: Routes = [
  {
    path: '',
    component: DeviceComponent,
    pathMatch: 'prefix',
    children:[
      {
        path: '',
        component: DevicesComponent,
        resolve: {
         summaries: DeviceListResolver
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeviceRoutingModule { }
