//Angular
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'appFilter'
})
export class FilterPipe implements PipeTransform {

  transform(items: any[], prop: string, value: string): any {
    if (items) {
      if (!value) {
        return items;
      } else {
        return items.filter(obj => this.filter(obj[prop], value));
      }
    } else {
      return [];
    }
  }

  filter(source: string, target: string): boolean {

    target = target.toLowerCase();
    return JSON.stringify(source).toLowerCase().includes(target);
  }
}

