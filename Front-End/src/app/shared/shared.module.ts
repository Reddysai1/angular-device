//Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//Third Parties 
import { NgxSpinnerModule } from 'ngx-spinner';

//Project 
import { FilterPipe } from './pipes/filter.pipe';


//Vendor modules should be defined here to use then in the application level
const VENDOR_MODULES_IMPORTED_EXPORTED: any[] = [
  CommonModule,
  NgxSpinnerModule,
  RouterModule,
  FormsModule,
  ReactiveFormsModule
];

const BASE_PIPES: any[] = [
  FilterPipe
];

@NgModule({
  declarations: [
    BASE_PIPES
  ],
  imports: [
    ...VENDOR_MODULES_IMPORTED_EXPORTED,
  ],
  exports: [
    ...VENDOR_MODULES_IMPORTED_EXPORTED,
    BASE_PIPES
  ],
  entryComponents: [],
  providers: []
})
export class SharedModule {

  constructor() { }
}
